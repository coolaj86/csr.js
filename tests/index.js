'use strict';

var Keypairs = require('@root/keypairs');
//var CSR = require('@root/csr');
var CSR = require('../csr.js');
//var PEM = require('@root/pem/packer');

async function run() {
	var pair = await Keypairs.generate();

	var hex = CSR.request({
		jwk: pair.public,
		domains: ['example.com', '*.example.com', 'foo.bar.example.com'],
		encoding: 'hex'
	});
	//console.log(hex);

	CSR.csr({
		jwk: pair.private,
		domains: ['example.com', '*.example.com', 'foo.bar.example.com'],
		encoding: 'pem'
	}).then(function(csr) {
		//var csr = PEM.packBlock({ type: 'CERTIFICATE REQUEST', bytes: der });
		console.log(csr);
    if (!/^-----BEGIN CERTIFICATE REQUEST-----\s*MII/m.test(csr)) {
      throw new Error("invalid CSR PEM");
    }
	  console.info('PASS: (if it looks right)');
	});
}

run();
